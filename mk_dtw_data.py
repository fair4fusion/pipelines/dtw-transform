import sys
import json
import os
import glob
# fname = sys.argv[1]


outputDir = "/test/data-out/"

# desired_files = "*.json"
# files = glob.glob(os.path.join(inputDir, desired_files))


os.chdir("/test/data-in/")
files = glob.glob("*.json")

for fname in files:
    f = open( fname, "r")
    d = json.load( f )
    vv = None

    try:
        vv = d["summary"]["time"]
    except:
        print( "Error in file {}: Something wrong with summary.global_quantities.time".format(fname) )
        continue

    try:
        siz_t = int(vv["size"])
    except:
        print( "Error in file {}: Something wrong with summary.global_quantities.time.size".format(fname) )
        continue
    try:
        t = vv["value"]
        assert len(t) == siz_t
    except:
        print( "Error in file {}: Something wrong with summary.global_quantities.time.value".format(fname) )
        continue



    variables = ["b0","ip"]

    for var in variables:

        try:
            vv = d["summary"]["global_quantities"][var]["value"]
        except:
            print( "Error in file {}: Something wrong with summary.global_quantities.{}.value".format(fname,var) )
            continue

        try:
            siz = int(vv["size"])
        except:
            print( "Error in file {}: Something wrong with summary.global_quantities.{}.value.size".format(fname,var) )
            continue

        try:
            v = vv["value"]
            assert len(v) == siz
        except:
            print( "Error in file {}: Something wrong with summary.global_quantities.{}.value.value".format(fname,var) )
            continue

        assert siz == siz_t
        fout = open(outputDir + fname+"_"+var+".list", "w+")
        for i in range(0,siz):
            fout.write( "{} {}\n".format(v[i],t[i]) )
        fout.close()

    #end for var in variables

exit(0)



