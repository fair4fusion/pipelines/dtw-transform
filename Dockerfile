FROM python:3

COPY mk_dtw_data.py mk_dtw_data.py

CMD [ "python", "mk_dtw_data.py" ]
